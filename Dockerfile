FROM tiangolo/uwsgi-nginx-flask

WORKDIR /app
COPY ./app.py /app/app.py
COPY ./requirements.txt /app/requirements.txt
COPY ./slack_secrets.yaml /app/slack_secrets.yaml
RUN pip3 install --upgrade -r /app/requirements.txt

CMD /app/app.py
